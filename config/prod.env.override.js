'use strict'
module.exports = {
  NODE_ENV: '"production"',
  API: '"https://insiderprotocol.com/api"',
  RECAPTCHA: '"6LdV6_UZAAAAAN6hcJPvX8wcc2PrJIEtaQwOlAXZ"',
  RECAPTCHA_V3: '"6Lc4oQMaAAAAAGC66NuYvF_ZJbK6UEw1czgESJpr"',
}
