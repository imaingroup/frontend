# Insider Protocol: Frontend

> Stack: git, docker, vue 2, eslint.

## Development

>apt install docker git node npm
git clone git@gitlab.com:btc_trading/frontend.git
cd frontend

run docker composer

>docker-compose up

Additional you may rebuild image:
>docker-compose up --build

Add package:
>docker exec -it frontend_frontend_dev_1 npm i yourDependency
