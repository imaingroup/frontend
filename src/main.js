// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue';
import App from './App';
import router from './router';
import store from './store';
import mixin from './mixin';
import './assets/scss/app.scss';
import checkView from 'vue-check-view';
import vuescroll from 'vuescroll';
import VueSocialSharing from 'vue-social-sharing';
import VueCookies from 'vue-cookies';
import {VueMaskDirective} from 'v-mask';

Vue.use(checkView);
Vue.use(vuescroll, {
  ops: {
    rail: {
      size: '.3em',
      background: '#2d2c3c',
      opacity: 1,
    },
    bar: {
      size: '.29em',
      background: '#4b4a59',
      keepShow: true,
    },
  },
});
Vue.directive('mask', VueMaskDirective);
Vue.use(VueSocialSharing);
Vue.use(VueCookies);
Vue.mixin(mixin);

Vue.config.productionTip = false;

import DefaultLayout from './components/layouts/Default.vue';
import EmptyLayout from './components/layouts/Empty.vue';
import AuthLayout from './components/layouts/Auth.vue';
import MessagesLayout from './components/layouts/Messages.vue';
import ErrorLayout from './components/layouts/Error.vue';

Vue.component('default-layout', DefaultLayout);
Vue.component('empty-layout', EmptyLayout);
Vue.component('auth-layout', AuthLayout);
Vue.component('messages-layout', MessagesLayout);
Vue.component('error-layout', ErrorLayout);

router.beforeResolve((to, from, next) => {
  if (to.name) {
    store.commit('showModal', false);
    document.body.classList.remove('open-menu');
  }
  next();
});

/* router.afterEach((to, from) => {
}); */

new Vue({
  el: '#app',
  router,
  store: store,
  components: {App},
  template: '<App/>',
});

Vue.prototype.$countAppErrors = 0;
