import Vue from 'vue';
import Router from 'vue-router';
import Index from '@/components/Index';

import Register from '@/components/Register';
import Login from '@/components/Login';
import Forgot from '@/components/Forgot';
import Reset from '@/components/Reset';
import Activate from '@/components/Activate';

import Dashboard from '@/components/Dashboard';
import Transactions from '@/components/Transactions';
import Investments from '@/components/Investments';
import Deposit from '@/components/Deposit';
import Withdraw from '@/components/Withdraw';
import Team from '@/components/Team';

import News from '@/components/News';
import NewsSingle from '@/components/NewsSingle';

import AccountInformation from '@/components/AccountInformation';
import Password from '@/components/Password';
import EmailNotifications from '@/components/EmailNotifications';
import TwoFactorAuthentication from '@/components/TwoFactorAuthentication';

import Messages from '@/components/Messages';

import Error404 from '@/components/404';

Vue.use(Router);

export default new Router({
  scrollBehavior(to, from, savedPosition) {
    return {x: 0, y: 0};
  },
  routes: [
    {
      path: '/',
      name: 'Index',
      component: Index,
      meta: {
        layout: 'empty-layout',
      },
    },
    {
      'path': '/register/:promocode?',
      'name': 'Register',
      'component': Register,
      'meta': {
        layout: 'auth-layout',
      },
    },
    {
      path: '/login',
      name: 'Login',
      component: Login,
      meta: {
        layout: 'auth-layout',
      },
    },
    {
      path: '/forgot',
      name: 'Forgot',
      component: Forgot,
      meta: {
        layout: 'auth-layout',
      },
    },
    {
      path: '/reset/:token',
      name: 'Reset',
      component: Reset,
      meta: {
        layout: 'auth-layout',
      },
    },
    {
      path: '/activate/:token',
      name: 'Activate',
      component: Activate,
      meta: {
        layout: 'auth-layout',
      },
    },
    {
      path: '/roadmap',
      name: 'RoadMap',
    },
    {
      path: '/news',
      component: {render(c) {
        return c('router-view');
      }},
      children: [
        {
          path: '/news',
          name: 'News',
          component: News,
        },
        {
          path: '/news/:id',
          name: 'NewsSingle',
          component: NewsSingle,
        },
      ],
    },
    {
      path: '/dashboard',
      component: {render(c) {
        return c('router-view');
      }},
      children: [
        {
          path: '/dashboard',
          name: 'Dashboard',
          component: Dashboard,
        },
        {
          path: '/dashboard',
          name: 'DashboardCard',
          component: Dashboard,
          meta: {
            anchor: 'dashboard__profits',
          },
        },
      ],
    },
    {
      path: '/investments',
      component: {render(c) {
        return c('router-view');
      }},
      children: [
        {
          path: '/investments',
          name: 'Investments',
          component: Investments,
          props: {openTid: false},
        },
        {
          path: '/investments',
          name: 'InvestmentsOpen',
          component: Investments,
          props: {openTid: true},
        },
      ],
    },
    {
      path: '/transactions',
      name: 'Transactions',
      component: Transactions,
    },
    {
      path: '/deposit',
      name: 'Deposit',
      component: Deposit,
    },
    {
      path: '/withdraw',
      name: 'Withdraw',
      component: Withdraw,
    },
    {
      path: '/team',
      name: 'Team',
      component: Team,
    },
    {
      path: '/profile',
      component: {render(c) {
        return c('router-view');
      }},
      children: [
        {
          path: '/profile',
          name: 'AccountInformation',
          component: AccountInformation,
        },
        {
          path: '/profile/password',
          name: 'Password',
          component: Password,
        },
        {
          path: '/profile/notifications',
          name: 'EmailNotifications',
          component: EmailNotifications,
        },
        {
          path: '/profile/2fa',
          name: 'TwoFactorAuthentication',
          component: TwoFactorAuthentication,
        },
      ],
    },
    {
      path: '/messages',
      name: 'Messages',
      component: Messages,
      meta: {
        layout: 'messages-layout',
      },
    },
    {
      path: '*',
      component: Error404,
      meta: {
        layout: 'error-layout',
      },
    },
  ],
});
