/**
 * This exception may be throws in MediaService.
 */
export class FormatNotSupportedException extends Error {
  // eslint-disable-next-line require-jsdoc
  constructor(...params) {
    super(params);

    if (Error.captureStackTrace) {
      Error.captureStackTrace(this, FormatNotSupportedException);
    }

    this.name = 'FormatNotSupportedException';
  }
}
