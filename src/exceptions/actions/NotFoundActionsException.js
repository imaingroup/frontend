/**
 * This exception may be throws
 * in vuex actions, if server
 * returned 404 status.
 */
export class NotFoundActionsException extends Error {
  // eslint-disable-next-line require-jsdoc
  constructor(...params) {
    super(params);

    if (Error.captureStackTrace) {
      Error.captureStackTrace(this, NotFoundActionsException);
    }

    this.name = 'NotFoundActionsException';
  }
}
