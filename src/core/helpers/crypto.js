export const Trackers = {
  'ethereum': 'https://etherscan.io',
  'ethclassic': 'https://etcblockexplorer.com',
  'bsc': 'https://bscscan.com',
  'velas': 'https://evmexplorer.velas.com',
  'matic': 'https://polygonscan.com',
  'klaytn': 'https://scope.klaytn.com',
};

export const NetworkNames = {
  'ethereum': 'Ethereum (ERC-20)',
  'bsc': 'Binance Smart Chain (BEP-20)',
  'matic': 'Matic (Polygon Mainnet)',
  'ethclassic': 'ETH Classic (ETC)',
  'velas': 'VELAS (VLX)',
  'klaytn': 'KLAYTN',
  'bitcoin': 'Bitcoin',
};
