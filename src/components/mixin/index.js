import errorToJSON from 'error-to-json';

export default {
  errorCaptured(err, vm, info) {
    this.$countAppErrors++;

    if (this.$countAppErrors > 100) {
      return;
    }

    if (process.env.NODE_ENV === 'development') {
      console.log('Error:', err);
      console.log('Info:', info);
      vm.$store.commit('showModal', true);
      vm.$store.commit('setModalContent', {
        title: 'Error',
        content: 'Something went wrong...',
        small: true,
        update: true,
      });
    } else {
      const logString = JSON.stringify(errorToJSON(err)) +
        ' | ' +
        window.location.href +
        ' | ' +
        navigator.userAgent;

      this.$store.dispatch('sendErrorLog', logString);

      return false;
    }
  },
  computed: {
    url() {
      return this.$store.state.url;
    },
    lang() {
      return this.$store.state.lang;
    },
    user() {
      return this.$store.state.user;
    },
    translates() {
      return this.$store.state.translates;
    },
    modal: {
      get() {
        return this.$store.state.modal;
      },
      set(value) {
        this.$store.commit('showModal', value);
      },
    },
    modalContent() {
      return this.$store.state.modalContent;
    },
    tac() {
      return this.$store.state.tac;
    },
    masks() {
      return this.$store.state.masks;
    },
    rateUpdate() {
      return this.$store.state.rateUpdate;
    },
  },
  mounted() {
    this.preparePage();
  },
  methods: {
    preparePage() {
      if (this.action) {
        this.$store.dispatch(this.action);
      }
      if (this.pageTitle) {
        this.$store.commit('setPageTitle', this.pageTitle);
      }
      if (this.breadcrumbs) {
        this.$store.commit('setBreadcrumbs', this.breadcrumbs);
      }
      this.prepareReCaptcha();
      window.addEventListener('resize', () => {
        this.prepareReCaptcha();
      });
    },
    scrollTo(block) {
      let top = 0;
      if (block == '.block3') {
        top = document.querySelector(block).parentNode.offsetTop;
      } else if (block == '.block4') {
        top = document.querySelector(block).offsetTop +
            document.querySelector(block).parentNode.offsetTop;
      } else {
        top = document.querySelector(block).offsetTop;
      }
      window.scroll({
        top: top,
        behavior: 'smooth',
      });
    },
    async checkContent(field = null, actions = null) {
      if (field && this[field] === null) {
        for (let i = 0; i < actions.length; i++) {
          await this.$store.dispatch(actions[i]);
        }
        window.timerContent = setTimeout(() => {
          clearTimeout(window.timerContent);
          this.updateContent();
        }, this.rateUpdate);
      } else {
        this.updateContent();
      }
    },
    async updateContent() {
      if (this.$route.name == 'Index' ||
          this.$route.meta.layout == 'auth-layout') return;
      const mask = this.$store.state.mask;
      // console.log(mask);
      const data = await this.$store.dispatch('accountCheckUpdate', mask);
      if (typeof data == 'object') {
        if (data.account_global) {
          this.$store.commit('setAccount', data.account_global.data);
        }
        if (data.dashboard_content) {
          this.$store.commit('setDashboardContents',
              data.dashboard_content.data);
        }
        if (data.news_categories) {
          this.$store.commit('setNewsCategories',
              data.news_categories.data.categories);
        }
        if (data.news_list) {
          this.$store.commit('setNews', data.news_list.data.news);
        }
        if (data.investments_balances) {
          this.$store.commit('setInvestmentsBalances',
              data.investments_balances.data.balances);
        }
        if (data.investments_chart) {
          this.$store.commit('setInvestmentsChart',
              data.investments_chart.data.chart);
        }
        if (data.investments_chart_by_trade) {
          this.$store.commit('setInvestmentsChart',
              data.investments_chart_by_trade.data.chart);
        }
        if (data.investments_options) {
          this.$store.commit('setOptions',
              data.investments_options.data.options);
        }
        if (data.investments_history) {
          this.$store.commit('setInvestments',
              data.investments_history.data.history);
        }
        if (data.transactions_list) {
          this.$store.commit('setTransactions',
              data.transactions_list.data.transactions);
        }
        if (data.deposit_history) {
          this.$store.commit('setDeposits',
              data.deposit_history.data.transactions);
        }
        if (data.withdraw_info) {
          this.$store.commit('setWithdrawInfo',
              data.withdraw_info.data.info);
        }
        if (data.withdraw_history) {
          this.$store.commit('setWithdraws',
              data.withdraw_history.data.history);
        }
        if (data.team_get) {
          this.$store.commit('setTeam', data.team_get.data.team);
        }
        if (data.contacts.chats) {
          this.$store.messages.commit('setContacts', data.contacts.chats);
        }
        if (data.chats) {
          this.$store.messages.commit('setChats', data.chats);
        }
        if (data.messages) {
          this.$store.messages.commit('setMessages', data.messages);
        }
      }
      window.timerContent = setTimeout(() => {
        clearTimeout(window.timerContent);
        this.updateContent();
      }, this.rateUpdate);
    },
    prepareReCaptcha() {
      const recaptchas = document.getElementsByClassName('form__recaptcha');
      let div = null;
      let container = null;
      let k = null;
      let count = 0;
      for (let i = 0; i < recaptchas.length; i++) {
        container = recaptchas[i];
        if (div = container.firstChild) {
          k = container.offsetWidth / div.offsetWidth;
          div.style.transform = 'scale(' + k + ')';
          container.style.height = div.offsetHeight * k + 'px';
          count++;
        }
      }
      if (count != recaptchas.length) {
        setTimeout(() => {
          this.prepareReCaptcha();
        }, 100);
      }
    },
    startPage() {
      return (this.pages.page - 1) * this.pages.limit + 1;
    },
    endPage() {
      const end = this.startPage() - 1 + this.pages.limit;
      return end > this.pages.total ? this.pages.total : end;
    },
    getString(string) {
      if (this.translates[string]) {
        return this.translates[string][this.lang];
      } else {
        return '';
      }
    },
    getErrors(errors) {
      return errors.map((item) => {
        return this.getString(item);
      }).join(', ');
    },
    prepareNumber(number, delimiter = ',', decimal = 2) {
      number = parseFloat(number);
      if (decimal) {
        number = parseFloat(number.toFixed(decimal));
      }
      return number.toString()
          .replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1' + delimiter);
    },
    prepareNumber1(number, delimiter = ',', decimal = 2) {
      number = parseFloat(number);
      if (decimal) {
        number = number.toFixed(decimal);
      }
      return number.toString()
          .replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1' + delimiter);
    },
    prepareNumber2(number, delimiter = ',', decimal = 2) {
      number = parseFloat(number).toString();
      for (let i = number.length; i < 8; i++) {
        number = '0' + number;
      }
      return number
          .replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1' + delimiter);
    },
    prepareProfitNumber(number, delimiter = ',', decimal = 2) {
      number = this.prepareNumber(number, delimiter, decimal);

      return parseFloat(number) > 0 ? '+' + number : number;
    },
    preparePersonalData(string, isHide, onlyFirst) {
      if (!isHide) {
        return string;
      } else if (string.length === 0) {
        return string;
      } else if (onlyFirst) {
        const sData = string.split('');
        return sData.splice(0, 1).join('') +
            sData.splice(0).join('').replace(/./g, '*');
      } else {
        const s = string.split('');
        const first = s.splice(0, 1);
        let last = '';
        if (s.indexOf('@')) {
          last = s.splice(s.lastIndexOf('.'), s.length).join('');
        } else {
          last = s.splice(s.length - 1, s.length);
        }
        const body = s.splice(1, s.length - 1)
            .map((item) => (item == '@' ? item : '*')).join('');
        return first + body + last;
      }
    },
    prepareTime(number) {
      number = Math.floor(number);
      if (number < 0) {
        number = 0;
      }
      return number < 10 ? '0' + number : number;
    },
    prepareChartDate(value) {
      return value.replace(/(.+)\/\d{4}/, '$1');
    },
    isNumber() {
      const charCode = (event.which) ? event.which : event.keyCode;
      if ((charCode > 31 &&
          (charCode < 48 || charCode > 57)) &&
          charCode !== 46) {
        event.preventDefault();
      } else {
        return true;
      }
    },
    setTimer() {
      const diff = this.timer.diff;
      this.timer.days = diff / (3600000 * 24);
      this.timer.hours = (diff % (3600000 * 24)) / 3600000;
      this.timer.minutes = (diff % 3600000) / (1000 * 60);
      this.timer.seconds =(diff % 60000) / 1000;
    },
    beginTimer() {
      const timer = this.timerDef;
      if (timer.is_enabled) {
        const currentTime = new Date(timer.current_time_utc.replace(' ', 'T'));
        const beforeAt = new Date(timer.before_at.replace(' ', 'T'));
        this.timer.diff = beforeAt - currentTime;
        setInterval(() => {
          this.timer.diff -= 1000;
          this.setTimer();
        }, 1000);
      }
    },
    loadAsObjectURL(video, url) {
      const xhr = new XMLHttpRequest();
      xhr.responseType = 'blob';
      xhr.onload = (response) => video.src = URL.createObjectURL(xhr.response);
      xhr.onerror = () => {/* Houston we have a problem */};
      xhr.open('GET', url, true);
      xhr.send();
      video.onload = () => URL.revokeObjectURL(video.src);
    },
    loadFlame(seeThru, fields = true) {
      this.flameButton(seeThru);
      if (fields) {
        this.flame = document.getElementById('inputFlame');
        const input = this.flame.querySelector('video');
        input.addEventListener('loadedmetadata', () => {
          seeThru.create(input);
          input.play();
        });
        this.loadAsObjectURL(input, require('@/assets/video/input.mp4'));
      }
      window.addEventListener('resize', () => {
        if (this.focused) {
          this.resizeFlame();
        }
      });
    },
    flameButton(seeThru, id = '#buttonAnimation') {
      const button = document.querySelector(id);
      button.addEventListener('loadedmetadata', () => {
        seeThru.create(button);
        button.play();
      });
      this.loadAsObjectURL(button, require('@/assets/video/button.mp4'));
    },
    activeFlame() {
      this.focused = event.target.previousSibling;
      this.focused.className = 'focused';
      this.flame.className = 'active';
      this.resizeFlame();
    },
    click2fa() {
      if (event.target.value.indexOf('_') !== -1) {
        event.target.setSelectionRange(0, event.target.value.indexOf('_'));
      }
    },
    deactiveFlame() {
      this.focused.className = '';
      this.flame.className = '';
      this.focused = null;
    },
    resizeFlame() {
      this.flame.style.width = this.focused.offsetWidth + 'px';
      this.flame.style.top = this.focused.parentNode.offsetTop + 'px';
      this.flame.style.left = this.focused.parentNode.offsetLeft + 'px';
    },
    showModal(title, content, scroll = false) {
      this.$store.commit('setModalContent', {
        title: title,
        content: content,
        small: true,
        scroll: scroll,
      });
      this.$store.commit('showModal', true);
    },
    mergeColors(...colors) {
      const colorMerger = function(...colors) {
        const hex2dec = function(hex) {
          return hex.replace('#', '').match(/.{2}/g).map((n) => parseInt(n, 16));
        };

        const rgb2hex = function(r, g, b) {
          r = Math.round(r);
          g = Math.round(g);
          b = Math.round(b);
          r = Math.min(r, 255);
          g = Math.min(g, 255);
          b = Math.min(b, 255);
          return '#' + [r, g, b].map((c) => c.toString(16).padStart(2, '0')).join('');
        };

        const rgb2cmyk = function(r, g, b) {
          let c = 1 - (r / 255);
          let m = 1 - (g / 255);
          let y = 1 - (b / 255);
          const k = Math.min(c, m, y);
          c = (c - k) / (1 - k);
          m = (m - k) / (1 - k);
          y = (y - k) / (1 - k);
          return [c, m, y, k];
        };

        const cmyk2rgb = function(c, m, y, k) {
          let r = c * (1 - k) + k;
          let g = m * (1 - k) + k;
          let b = y * (1 - k) + k;
          r = (1 - r) * 255 + .5;
          g = (1 - g) * 255 + .5;
          b = (1 - b) * 255 + .5;

          if (isNaN(r) || isNaN(g) || isNaN(b)) {
            throw new MergerException('Incorrect parameters');
          }

          return [r, g, b];
        };

        const mixCmyks = function(...cmyks) {
          const c = cmyks.map((cmyk) => cmyk[0]).reduce((a, b) => a + b, 0) / cmyks.length;
          const m = cmyks.map((cmyk) => cmyk[1]).reduce((a, b) => a + b, 0) / cmyks.length;
          const y = cmyks.map((cmyk) => cmyk[2]).reduce((a, b) => a + b, 0) / cmyks.length;
          const k = cmyks.map((cmyk) => cmyk[3]).reduce((a, b) => a + b, 0) / cmyks.length;
          return [c, m, y, k];
        };

        const mixHexes = function(...hexes) {
          const rgbs = hexes.map((hex) => hex2dec(hex));
          const cmyks = rgbs.map((rgb) => rgb2cmyk(...rgb));
          const mixtureCmyk = mixCmyks(...cmyks);
          const mixtureRgb = cmyk2rgb(...mixtureCmyk);
          return rgb2hex(...mixtureRgb);
        };

        return mixHexes(...colors);
      };

      const MergerException = function(message) {
        this.message = message;
        this.name = 'MergerException';
      };

      return colorMerger(...colors);
    },
  },
};
