import store from '../store/';
import errorToJSON from 'error-to-json';

/**
 * Logger.
 */
export default class Logger {
  /**
   * Register exception.
   *
   * @param {Error} err
   * @param {String|null} info
   */
  static async registerException(err, info) {
    try {
      store.commit('setCountAppErrors', store.state.countAppErrors + 1);

      if (process.env.NODE_ENV === 'development') {
        console.log('Error:', err);
        console.log('Info:', info);
        store.commit('showModal', true);
        store.commit('setModalContent', {
          title: 'Error',
          content: 'Something went wrong 1234...',
          small: true,
          update: true,
        });
      } else {
        if (store.state.countAppErrors > 100) {
          return;
        }

        const logString = JSON.stringify(errorToJSON(err)) +
          ' | ' +
          window.location.href +
          ' | ' +
          navigator.userAgent;

        await store.dispatch('sendErrorLog', logString);
      }
    } catch (e) {
      if (process.env.NODE_ENV === 'development') {
        console.log('Logger.registerException crashed.');
      }
    }
  }

  /**
   * Register exceptions.
   *
   * @param {Error[]} errors
   */
  static async registerExceptions(errors) {
    for (const error of errors) {
      await this.registerException(error);
    }
  }
};
