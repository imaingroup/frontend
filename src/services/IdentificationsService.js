import FingerprintJS from '@fingerprintjs/fingerprintjs';
import 'clientjs';

export default {
  data() {
    return {
      cookieUniqueId: 'ulsipiv',
    };
  },
  methods: {
    async sendIdentificationInfo(userid) {
      const fp = await FingerprintJS.load();
      const visitorInfo = await fp.get();
      const clientjs = new ClientJS();
      const randomString = this.randomString(16);
      const request = {
        'userid': userid,
        'id1': visitorInfo.visitorId,
        'id2': clientjs.getFingerprint(),
        'user_agent': clientjs.getUserAgent(),
        'browser': clientjs.getBrowser(),
        'browser_version': clientjs.getBrowserVersion(),
        'browser_major_version': clientjs.getBrowserMajorVersion(),
        'is_ie': clientjs.isIE(),
        'is_chrome': clientjs.isIE(),
        'is_firefox': clientjs.isFirefox(),
        'is_safari': clientjs.isSafari(),
        'is_opera': clientjs.isOpera(),
        'engine': clientjs.getEngine(),
        'engine_version': clientjs.getEngineVersion(),
        'platform': visitorInfo.components.platform.value,
        'os': clientjs.getOS(),
        'os_version': clientjs.getOSVersion(),
        'is_windows': clientjs.isWindows(),
        'is_linux': clientjs.isLinux(),
        'is_ubuntu': clientjs.isUbuntu(),
        'is_solaris': clientjs.isSolaris(),
        'device': clientjs.getDevice(),
        'device_type': clientjs.getDeviceType(),
        'device_vendor': clientjs.getDeviceVendor(),
        'cpu': clientjs.getCPU(),
        'is_mobile': clientjs.isMobile(),
        'is_mobile_android': clientjs.isMobileAndroid(),
        'is_mobile_opera': clientjs.isMobileOpera(),
        'is_mobile_windows': clientjs.isMobileWindows(),
        'is_mobile_blackberry': clientjs.isMobileBlackBerry(),
        'is_mobile_ios': clientjs.isMobileIOS(),
        'is_iphone': clientjs.isIphone(),
        'is_ipod': clientjs.isIpod(),
        'color_depth': clientjs.getColorDepth(),
        'current_resolution': clientjs.getCurrentResolution(),
        'available_resolution': clientjs.getAvailableResolution(),
        'device_xdpi': clientjs.getDeviceXDPI(),
        'device_ydpi': clientjs.getDeviceYDPI(),
        'plugins': clientjs.getPlugins(),
        'is_java': clientjs.isJava(),
        'java_version': clientjs.getJavaVersion(),
        'is_flash': clientjs.isFlash(),
        'flash_version': clientjs.getFlashVersion(),
        'is_silverlight': clientjs.isSilverlight(),
        'silverlight_version': clientjs.getSilverlightVersion(),
        'mime_types': clientjs.getMimeTypes(),
        'is_mime_types': clientjs.isMimeTypes(),
        'is_font': clientjs.isFont(),
        'fonts': clientjs.getFonts(),
        'is_local_storage': clientjs.isLocalStorage(),
        'is_session_storage': clientjs.isSessionStorage(),
        'is_cookie': clientjs.isCookie(),
        'time_zone': visitorInfo.components.timezone.value,
        'language': clientjs.getLanguage(),
        'languages': visitorInfo.components.languages.value.join(),
        'system_language': clientjs.getSystemLanguage(),
        'is_canvas': clientjs.isCanvas(),
        'canvas_hash': this.cyrb53(clientjs.getCanvasPrint()),
        'local_storage': this.localStorageUnique(randomString),
        'cookie': this.cookieUnique(randomString),
      };

      await this.$store.dispatch('identificationsSend', request);
    },
    localStorageUnique(id) {
      try {
        const ls = localStorage || window.localStorage;
        const key = this.cookieUniqueId;

        if (!ls.getItem(key)) {
          ls.setItem(key, id);
        }

        return ls.getItem(key);
      } catch (e) {
        return null;
      }
    },
    cookieUnique(id) {
      if (!this.$cookies.get(this.cookieUniqueId)) {
        this.$cookies.set(this.cookieUniqueId, id, new Date(9999, 0, 0));
      }

      return this.$cookies.get(this.cookieUniqueId);
    },
  },
};
