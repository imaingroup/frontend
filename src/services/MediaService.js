import {FormatNotSupportedException} from '../exceptions/services/FormatNotSupportedException';

export default {
  data() {
    return {
      videoFormats: ['av1', 'mp4v', 'mp4', 'm4p', '3gp', '3g2', 'mpg', 'mp2', 'mpe', 'mpe', 'mpv', 'm2v', 'ogv', 'ogg', 'webm'],
      imageFormats: ['apng', 'avif', 'gif', 'jpg', 'jpeg', 'jfif', 'pjpeg', 'pjp', 'png', 'svg', 'webp'],
    };
  },
  methods: {
    showModalMedia(link, openLinkOther) {
      const type = this.getMediaType(link);

      if (!type) {
        if (openLinkOther) {
          window.open(link, '_blank')
              .focus();

          return;
        }

        throw new FormatNotSupportedException();
      }

      let contentData = null;

      if (type === 'video') {
        contentData = '<video src="' + link + '" controls autoplay />';
      } else if (type === 'image') {
        contentData = '<img src="' + link + '"  alt=""/>';
      } else if (type === 'youtube') {
        contentData = '<iframe width="100%" height="100%" style=" min-height: 25em" src="https://www.youtube.com/embed/'+
          this.getYoutubeId(link)+'&loop=1" allowfullscreen=""></iframe>';
      }

      this.$store.commit('setModalContent', {
        content: contentData,
        big: true,
      });

      this.$store.commit('showModal', true);
    },
    getMediaType(link) {
      const extension = this.getExtension(link);

      if (extension) {
        if (this.videoFormats.includes(extension)) {
          return 'video';
        } else if (this.imageFormats.includes(extension)) {
          return 'image';
        }
      }

      if (this.getYoutubeId(link)) {
        return 'youtube';
      }

      return null;
    },
    getExtension(link) {
      const type = /[^.]+$/.exec(link.toLowerCase());

      return type && type.length > 0 ? type[0] : null;
    },
    getYoutubeId(link) {
      const p = /^(?:https?:\/\/)?(?:www\.)?youtube\.com\/watch\?(?=.*v=((\w|-){11}))(?:\S+)?$/;

      return (link.match(p)) ? RegExp.$1 : null;
    },
  },
};
