import {VueReCaptcha} from 'vue-recaptcha-v3';
import Vue from 'vue';

export default {
  methods: {
    async getRecaptchaToken() {
      await this.$recaptchaLoaded();
      return await this.$recaptcha('login');
    },
  },
  async created() {
    if (!this.$store.state.recaptchaV3Init) {
      this.$store.commit('setRecaptchaV3Init', true);

      Vue.use(VueReCaptcha,
          {
            siteKey: this.$store.state.reCaptchaKeyV3,
            loaderOptions: {
              autoHideBadge: true,
            },
          }
      );
    }
  },
};
