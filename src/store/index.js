import Vue from 'vue';
import Vuex from 'vuex';
import createMutationsSharer from 'vuex-shared-mutations';
import EventEmitter from 'events';

import * as getters from './getters';
import * as mutations from './mutations';
import * as actions from './actions';

import {core} from './modules/core';
import {accounts} from './modules/accounts';
import {messages} from './modules/messages';

Vue.use(Vuex);

const store = new Vuex.Store({
  state: {
    reCaptchaKey: process.env.RECAPTCHA,
    reCaptchaKeyV3: process.env.RECAPTCHA_V3,
    lang: 'en',
    langs: [],
    user: null,
    styles: null,

    modal: false,
    modalContent: null,

    pageTitle: '',
    breadcrumbs: [],
    tac: {
      title: 'Terms & Conditions',
      text: '',
    },

    translates: {},

    notifications: [],

    newsIndex: {},
    news: null,
    newsSingle: null,
    newsParams: {
      sort: 'DESC',
      category: null,
      search: null,
      page: 1,
    },
    newsCategories: null,
    timer: {},

    abc: {
      abc_left: 0,
      abc_left_title: {},
      abc_sold: 0,
      abc_sold_title: {},
      soft_cap: 0,
      soft_cap_title: {},
      total_supply: 0,
      total_supply_title: {},
    },

    products: [],
    social: [],

    transactions: null,
    investments: null,
    balances: {},
    options: [],
    investmentsChart: null,
    deposits: null,
    withdraws: null,

    landing: {},
    dashboard: null,
    deposit: {},
    withdraw: {},
    profile: {},
    twofa: {},
    team: null,

    masks: {},
    mask: null,
    rateUpdate: 5000,
    timerAccount: null,
    timerContent: null,

    tid: 0,

    countAppErrors: 0,

    recaptchaV3Init: false,
    recaptchaV3ChangedToV2: false,

    ee: new EventEmitter(),

    newMessages: 0,

    confirmedVersion: {},
  },
  getters,
  mutations,
  actions,
  modules: {
    core,
    accounts,
    messages,
  },
  plugins: [
    createMutationsSharer({
      predicate: [
        'setLang',
        'setUser',
        'setAccount',
        'setNotifications',
        'setLandingContents',
        'setDashboardContents',
        'setDepositInfo',
        'setWithdrawInfo',
        'setProfileContent',
        'setTransactions',
        'setNews',
        'setNewsParams',
        'setInvestments',
        'setInvestmentsBalances',
        'setOptions',
        'setInvestmentsChart',
        'setDeposits',
        'setWithdraws',
        'set2fa',
        'setTeam',

        'messages/setContacts',
        'messages/setChats',
        'messages/setChat',
        'messages/setMessages',
      ],
    }),
  ],
});

export default store;
