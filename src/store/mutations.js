import router from '../router';
import {set} from './helpers/mutations';

const setPageTitle = set('pageTitle');
const setBreadcrumbs = set('breadcrumbs');

const showModal = set('modal');
const setModalContent = set('modalContent');

const setLang = set('lang');
const setLangs = set('langs');

const setUser = set('user');
const setStyles = set('styles');
const setMessages = set('newMessages');

const setTranslates = (store, data) => {
  store.translates = {
    ...store.translates,
    ...data,
  };
};

const setAccount = (store, data) => {
  setUser(store, data.user);
  setStyles(store, data.styles);
  setLang(store, data.language);
  setLangs(store, data.langs);
  setTranslates(store, data.translates);
  setNotifications(store, data.notifications);
  setMasks(store, data.masks);
  setRateUpdate(store, data.rate_update);
  setMessages(store, data.new_messages);
  if (router.currentRoute.name === 'Login') {
    router.push({name: 'Dashboard'});
  }
};
const setNotifications = set('notifications');

const setLandingContents = (store, data) => {
  setLang(store, data.settings.language);
  setLangs(store, data.settings.langs);
  setTranslates(store, data.settings.translates);

  store.newsIndex = data.news;

  store.products = data.settings.links
      .filter((item) => item.is_product == 1);
  store.social = data.settings.links
      .filter((item) => item.is_social == 1);

  store.abc = data.settings.abc;
  store.timer = {
    ...data.settings.timer,
    current_time_utc: data.settings.current_time_utc,
  };

  store.landing = data.settings.landing;
  store.translates['landing.about.our_bot_text'].en =
      'Combines different patterns in algorithms, but the main | ' +
      'patterns are High-Frequency Trading (HFT) and Positional Trading | ' +
      'implemented in the LAYERING trading technique which is used mostly by' +
      ' | the financial sector. This trading technique for ordinary people' +
      ' has  | been recognized illegal since 2013 when all financial brokers' +
      ' (and the | entire banking sector) began to suffer large losses in' +
      ' the form of | lost daily profits.';
  store.translates['landing.about.xchange_text_1'].en =
      'We are developing our own decentralized exchange which | ' +
      ' will be combined with the current ecosystem and the trading fees | ' +
      'will be lower than on existing exchanges. Also, there will be no | ' +
      'funding fees either. Nowadays none of the existing exchanges is ' +
      ' | decentralized and made not for users. We are against this approach.';
  store.translates['landing.about.xchange_text_2'].en =
      'Step one has already been made and Bitcoin was created in 2009. Now | ' +
      'time for the second step in the form of creating a true ' +
      ' decentralized | anonymous exchange.';
};
const setDashboardContents = (store, data) => {
  if (data) {
    if (data.abc) {
      store.abc = {
        ...data.abc,
        usdtRate: data.abc_usdt_rate,
      };
    }

    if (data.timer) {
      store.timer = {
        ...data.timer,
        current_time_utc: data.current_time_utc,
      };
    }
  }

  store.dashboard = data;
};
const setDepositInfo = set('deposit');
const setWithdrawInfo = set('withdraw');
const setProfileContent = set('profile');

const setTransactions = set('transactions');
const setNewsCategories = set('newsCategories');
const setNews = set('news');
const setNewsSingle = set('newsSingle');
const setNewsParams = (store, param) => {
  store.newsParams = {
    ...store.newsParams,
    ...param,
  };
};

const setInvestments = set('investments');
const setInvestmentsBalances = set('balances');
const setOptions = set('options');
const setInvestmentsChart = set('investmentsChart');
const setDeposits = set('deposits');
const setWithdraws = set('withdraws');
const set2fa = set('twofa');
const setTeam = set('team');
const setTac = (store, text) => {
  store.tac.text = text;
};
const setMask = set('mask');
const setMasks = set('masks');
const setRateUpdate = set('rateUpdate');

const setCountAppErrors = set('countAppErrors');

const setRecaptchaV3Init = set('recaptchaV3Init');
const setRecaptchaV3ChangedToV2 = set('recaptchaV3ChangedToV2');

const clearAll = (store) => {
  setUser(store, null);
  setLang(store, 'en');
  setNotifications(store, []);
  setMasks(store, {});
  setMessages(store, 0);
};

const setConfirmedVersion = (store, {mask, version}) => {
  store.confirmedVersion[mask] = version;
};

export {
  setPageTitle,
  setBreadcrumbs,

  showModal,
  setModalContent,

  setLang,
  setLangs,

  setUser,
  setStyles,
  setTranslates,

  setAccount,
  setNotifications,

  setLandingContents,
  setDashboardContents,
  setDepositInfo,
  setWithdrawInfo,
  setProfileContent,

  setTransactions,
  setNewsCategories,
  setNews,
  setNewsSingle,
  setNewsParams,
  setInvestments,
  setInvestmentsBalances,
  setOptions,
  setInvestmentsChart,
  setDeposits,
  setWithdraws,
  set2fa,
  setTeam,
  setTac,

  setMask,
  setMasks,
  setRateUpdate,

  setCountAppErrors,

  setRecaptchaV3Init,
  setRecaptchaV3ChangedToV2,

  clearAll,

  setConfirmedVersion,
};
