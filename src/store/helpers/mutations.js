export const set = (property) => (store, payload) => (store[property] = payload);
