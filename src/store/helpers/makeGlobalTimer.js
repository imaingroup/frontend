export const makeGlobalTimer = (freq) => {
  freq = freq || 1000;

  const callbacks = [];

  let id = setTimeout(() => {
    let idx;
    for (idx in callbacks) {
      if (Object.prototype.hasOwnProperty.call(callbacks, idx)) {
        callbacks[idx]();
      }
    }
  }, freq);

  return {
    'id': function() {
      return id;
    },
    'registerCallback': function(cb) {
      callbacks.push(cb);
    },
    'cancel': function() {
      if (id !== null) {
        clearTimeout(id);
        id = null;
      }
    },
  };
};
