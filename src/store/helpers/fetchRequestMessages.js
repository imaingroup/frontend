import {makeGlobalTimer} from './makeGlobalTimer';
import store from '../../store/index';
import router from '../../router';

export const fetchRequest = async (
  url,
  params = {},
  headers = {},
  disable403
) => {
  const result = {
    status: true,
    data: {},
    ok: null,
  };

  const mergedHeaders = Object.assign({
    'Accept': 'application/json',
  }, headers);

  const pt = makeGlobalTimer(2000);
  const st = makeGlobalTimer(20000);

  if (url.indexOf('account/check/update') == -1) {
    pt.registerCallback(() => {
      document.body.classList.add('preloading');
    });

    st.registerCallback(() => {
      store.commit('showModal', true);
      store.commit('setModalContent', {
        title: 'Error',
        content: 'Slow internet connection...',
        small: true,
        update: true,
      });
    });
  }

  try {
    const response = await fetch(url, {
      ...params,
      headers: {
        ...mergedHeaders,
      },
      credentials: 'include',
    });

    result.ok = response.ok;

    store.state.ee.emit('fetch_request.done', response);
    result.status = response.status;

    if (result.status === 401) {
      router.push({name: 'Login'});
    } else if (!disable403 && result.status === 403) {
      store.commit('showModal', true);
      store.commit('setModalContent', {
        title: 'Not allowed',
        content: 'Operation has already been performed, or no access rights.',
        small: true,
      });
    } else if (response.status == 500) {
      if (!url.includes('/user/info')) {
        store.commit('showModal', true);
        store.commit('setModalContent', {
          title: 'Server error',
          content: 'The server can`t process this request.' +
            '<br>Our support team received a notification ' +
            'about this error and will solve this issue shortly.' +
            '<br><br>Thank you for your patience and understanding!',
          small: true,
        });
      }
    }

    const text = await response.text();
    let data = null;

    if (response.status == 503) {
      try {
        data = JSON.parse(text);

        if (typeof(data.maintance) !== 'undefined') {
          throw new Error();
        }
      } catch (e) {
        if (process.env.NODE_ENV !== 'development') {
          window.location.reload();
        }
        return;
      }
    }

    result.data = data ? data : JSON.parse(text);
  } catch (error) {
    console.log(error);
  }

  document.body.classList.remove('preloading');
  pt.cancel();
  st.cancel();

  return result;
};
