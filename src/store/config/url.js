let url = process.env.API;

if (process.env.API_AUTO) {
  url = window.location.protocol
      .concat('//')
      .concat(window.location.host)
      .concat('/api');
}

export default url;
