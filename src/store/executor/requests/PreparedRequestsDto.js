// eslint-disable-next-line no-unused-vars
import {PreparedRequestDto} from './PreparedRequestDto';
import {MultipleRouteOneTypeNotSupportedException} from '../exceptions/MultipleRouteOneTypeNotSupportedException';

/**
 * Prepared request DTO.
 *
 * @property {Number} always
 * @property {PreparedRequestDto[]} methods
 */
export class PreparedRequestsDto {
  /**
   * Constructor.
   *
   * @param {Boolean} always
   * @param {PreparedRequestDto[]} preparedRequests
   */
  constructor(always, preparedRequests) {
    this.validateDuplicates(preparedRequests);
    this.changeAlways(always);
    this.methods = preparedRequests;
  };

  /**
   * Validate duplicates.
   *
   * @param {PreparedRequestDto[]} preparedRequests
   * @throws MultipleRouteOneTypeNotSupportedException
   */
  validateDuplicates(preparedRequests) {
    const list = [];
    for (const request of preparedRequests) {
      if (list.includes(request.route)) {
        throw new MultipleRouteOneTypeNotSupportedException();
      }

      list.push(request.route);
    }
  }

  /**
   * Change always.
   *
   * @param {Boolean} always
   */
  changeAlways(always) {
    this.always = always ? 1 : 0;
  }

  /**
   * Find by route.
   *
   * @param {String} route
   * @return {PreparedRequestDto[]}
   */
  findByRoute(route) {
    return this.methods.filter((request) => {
      return request.route === route;
    });
  }

  /**
   * Add or replace by route name.
   *
   * @param {PreparedRequestDto} preparedRequest
   */
  addOrReplace(preparedRequest) {
    let changed = false;

    this.methods.forEach((request, index ) => {
      if (!changed && request.isRoute(preparedRequest.route)) {
        this.methods[index] = preparedRequest;
        changed = true;
      }
    });

    if (!changed) {
      this.methods.push(preparedRequest);
    }
  }

  /**
   * Remove prepared request from list.
   *
   * @param {PreparedRequestDto} preparedRequest
   */
  removeRequest(preparedRequest) {
    this.methods = this.methods.filter((request) => {
      return request !== preparedRequest;
    });
  }

  /**
   * Remove prepared request by route name.
   *
   * @param {String} route
   */
  removeByRoute(route) {
    this.methods = this.methods.filter((request) => {
      return !request.isRoute(route);
    });
  }
}
