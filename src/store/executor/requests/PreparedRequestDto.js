import Crc32 from '../classes/Crc32';
const weakMap = new WeakMap();
/**
 * Prepared request DTO.
 *
 * @property {string} route
 * @property {[]} parameters
 * @property {Object} request
 * @property {BigInt} version
 */
export class PreparedRequestDto {
  /**
   * Constructor.
   *
   * @param {String} route
   * @param {[]} parameters
   * @param {Object} request
   * @param {Object|null} options
   */
  constructor(route, parameters = [], request = {}, options = {}) {
    weakMap.set(this, {
      'route': route,
      'parameters': parameters,
      'request': request,
      'version': 1,
      'options': options,
      'destroy': false,
    });
  }

  /**
   * Get route.
   *
   * @return {String}
   */
  get route() {
    return weakMap.get(this).route;
  }

  /**
   * Get parameters.
   *
   * @return {[]}
   */
  get parameters() {
    return weakMap.get(this).parameters;
  }

  /**
   * Set parameters.
   *
   * @param {[]} parameters
   */
  set parameters(parameters) {
    weakMap.get(this).parameters = parameters;
  }

  /**
   * Get request.
   *
   * @return {String}
   */
  get request() {
    return weakMap.get(this).request;
  }

  /**
   * Get version.
   *
   * @return {String}
   */
  get version() {
    return weakMap.get(this).version;
  }

  /**
   * Get options.
   *
   * @return {Object}
   */
  get options() {
    return weakMap.get(this).options;
  }

  /**
   * Get version store name.
   *
   * @return {String}
   */
  get storeVersionName() {
    return this.route
        .concat('_')
        .concat(Crc32.crc32(JSON.stringify([this.parameters, this.request])).toString());
  }

  /**
   * Get destroy.
   *
   * @return {Object}
   */
  get destroy() {
    return weakMap.get(this).destroy;
  }

  /**
   * Change version.
   *
   * @param {BigInt} version
   */
  setVersion(version) {
    weakMap.get(this).version = version;
  }

  /**
   * If request mark as destroy,
   * it not apply for updates
   * and will remove from request
   * list at next update tick.
   */
  setDestroy() {
    weakMap.get(this).destroy = true;
  }

  /**
   * Check route equals.
   *
   * @param {String} route
   * @return {Boolean}
   */
  isRoute(route) {
    return this.route === route;
  }

  /**
   * To json.
   *
   * @return {Object}
   */
  toJSON() {
    return {
      'route': this.route,
      'parameters': this.parameters,
      'request': this.request,
      'version': this.version,
    };
  }
}
