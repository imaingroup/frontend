import {OldVersionException} from '../exceptions/OldVersionException';

const weakMap = new WeakMap();
import store from '../../index';

/**
 * Abstract processor.
 */
export class AbstractProcessor {
  /**
   * Constructor.
   *
   * @param {Object} response
   * @param {PreparedRequestDto} request
   * @throws {OldVersionException}
   */
  constructor(response, request) {
    weakMap.set(this, {
      'response': response,
      'request': request,
    });

    this.updateVersion();
  }

  /**
   * Update version data.
   *
   * @throws {OldVersionException}
   */
  updateVersion() {
    const keyVersion = this.request.storeVersionName;

    if (this.response && this.response.version && !this.request.destroy) {
      if (this.response.version < store.state.core.versions[keyVersion]) {
        throw new OldVersionException();
      }

      if (store.state.core.versions[keyVersion] < this.response.version) {
        store.commit('core/setDataVersion', {
          name: keyVersion,
          version: this.response.version,
        });
      }
    }
  }

  /**
   * Get response.
   *
   * @return {Object}
   */
  get response() {
    return weakMap.get(this)
        .response;
  }

  /**
   * Get request.
   *
   * @return {PreparedRequestDto}
   */
  get request() {
    return weakMap.get(this)
        .request;
  }
}
