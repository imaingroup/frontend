import {AbstractProcessor} from '../AbstractProcessor';
import store from '../../../index';

/**
 * AccountsGetProcessor.
 */
export class AccountsGetProcessor extends AbstractProcessor {
  /**
   * Process data.
   */
  process() {
    if (this.response.status === 200) {
      store.commit('setAccount', this.response.response.data);
    }
  }
}
