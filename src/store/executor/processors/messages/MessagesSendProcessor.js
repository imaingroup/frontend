import {AbstractProcessor} from '../AbstractProcessor';

/**
 * MessagesSendProcessor.
 */
export class MessagesSendProcessor extends AbstractProcessor {
  /**
   * Process data.
   */
  process() {}
}
