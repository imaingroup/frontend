import {AbstractProcessor} from '../../AbstractProcessor';
import store from '../../../../index';

/**
 * MessagesContactsGetProcessor.
 */
export class MessagesContactsGetProcessor extends AbstractProcessor {
  /**
   * Process data.
   */
  process() {
    if (this.response.status === 200) {
      store.commit('messages/setContacts', this.response.response.data.contacts.chats);
      store.commit('messages/setLegends', this.response.response.data.contacts.legend);
    }
  }
}
