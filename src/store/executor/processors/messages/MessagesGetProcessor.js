import {AbstractProcessor} from '../AbstractProcessor';
import store from '../../../index';

/**
 * MessagesGetProcessor.
 */
export class MessagesGetProcessor extends AbstractProcessor {
  /**
   * Process data.
   */
  process() {
    if (this.response.status !== 200) {
      return;
    }

    const currentChat = store.state.messages.currentChat;

    if (currentChat && currentChat.id === this.response.response.data.source_id) {
      this.updateMessages();
      this.scrolling();
    } else if (!currentChat) {
      this.request.setDestroy();
    }
  }

  /**
   * Update messages in store.
   */
  updateMessages() {
    if (this.request.parameters.length === 3 && this.request.parameters[1] === 1) {
      const existentMessages = store.state.messages.chatMessages;

      try {
        if (existentMessages && existentMessages.length > 0 && existentMessages[existentMessages.length - 1].id === this.request.parameters[2]) {
          store.commit('messages/setMessages', [...existentMessages, ...this.response.response.data.messages]);
        }
      } catch (e) {}
    } else if (this.request.parameters.length === 1 ||
      (this.request.parameters.length === 2 && this.request.parameters[1] === 1)
    ) {
      store.commit('messages/setMessages', this.response.response.data.messages);
    }
  }

  /**
   * Auto scrolling to bottom.
   */
  scrolling() {
    if (this.request.parameters.length === 1 ||
      (this.request.parameters.length > 1 && this.request.parameters[1] === 1)
    ) {
      const area = document.querySelector('.messages__content__area .messages__scroll');

      if (area) {
        const scrollToBottom = area.scrollHeight - area.scrollTop === area.clientHeight;

        if (scrollToBottom) {
          area.scrollTop = area.scrollHeight;
        }
      }
    }
  }
}
