import {AbstractProcessor} from '../AbstractProcessor';
import store from '../../../index';

/**
 * MessagesObserveProcessor.
 */
export class MessagesObserveProcessor extends AbstractProcessor {
  /**
   * Process data.
   */
  process() {
    if (this.response.status !== 200) {
      return;
    }

    const currentChat = store.state.messages.currentChat;
    const data = this.response.response.data;

    if (currentChat && currentChat.id === data.source_id && data.messages.length > 0) {
      const existentMessages = store.state.messages.chatMessages;

      for (const updatedMessage of data.messages) {
        if (updatedMessage.deleted) {
          const found = existentMessages.find((message) => message.id === updatedMessage.id);

          if (found) {
            existentMessages.splice(existentMessages.indexOf(found), 1);
          }
        } else {
          const oldMessage = existentMessages.find((message) => message.id === updatedMessage.id);

          if (oldMessage) {
            Object.assign(oldMessage, updatedMessage);
          }
        }
      }
    }
  }
}
