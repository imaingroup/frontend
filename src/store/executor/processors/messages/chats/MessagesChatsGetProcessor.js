import {AbstractProcessor} from '../../AbstractProcessor';
import store from '../../../../index';

/**
 * MessagesChatsGetProcessor.
 */
export class MessagesChatsGetProcessor extends AbstractProcessor {
  /**
   * Process data.
   */
  process() {
    if (this.response.status === 200) {
      store.commit('messages/setChats', this.response.response.data.chats);
    }
  }
}
