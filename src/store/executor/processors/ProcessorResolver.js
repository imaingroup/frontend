import {MessagesContactsGetProcessor} from './messages/contacts/MessagesContactsGetProcessor';
import {RouteNotResolvedException} from '../exceptions/RouteNotResolvedException';
import {MessagesChatsGetProcessor} from './messages/chats/MessagesChatsGetProcessor';
import {AccountsGetProcessor} from './accounts/AccountsGetProcessor';
import {MessagesSendProcessor} from './messages/MessagesSendProcessor';
import {MessagesGetProcessor} from './messages/MessagesGetProcessor';
import Routes from '../Routes';
import {MessagesObserveProcessor} from './messages/MessagesObserveProcessor';

const mapData = new Map();
mapData.set(Routes.ACCOUNT_GLOBAL, AccountsGetProcessor);
mapData.set(Routes.MESSAGES_CONTACTS_GET, MessagesContactsGetProcessor);
mapData.set(Routes.MESSAGES_CHATS_GET, MessagesChatsGetProcessor);
mapData.set(Routes.MESSAGES_SEND, MessagesSendProcessor);
mapData.set(Routes.MESSAGES_GET, MessagesGetProcessor);
mapData.set(Routes.MESSAGES_OBSERVE, MessagesObserveProcessor);

/**
 * ProcessorResolver.
 */
export default new class ProcessorResolver {
  // eslint-disable-next-line valid-jsdoc
  /**
   * Get resolver by route.
   *
   * @param {String} route
   * @return {String}
   */
  resolve(route) {
    if (!mapData.has(route)) {
      throw new RouteNotResolvedException();
    }

    return mapData.get(route);
  }
};
