/**
 * Routes.
 */
export default class Routes {
  /**
   * ACCOUNT_GLOBAL.
   *
   * @return {string}
   */
  static get ACCOUNT_GLOBAL() {
    return 'account.global';
  }

  /**
   * MESSAGES_CONTACTS_GET.
   *
   * @return {string}
   */
  static get MESSAGES_CONTACTS_GET() {
    return 'messages.contacts.get';
  }

  /**
   * MESSAGES_CHATS_GET.
   *
   * @return {string}
   */
  static get MESSAGES_CHATS_GET() {
    return 'messages.chats.get';
  }

  /**
   * MESSAGES_SEND.
   *
   * @return {string}
   */
  static get MESSAGES_SEND() {
    return 'messages.send';
  }

  /**
   * MESSAGES_GET.
   *
   * @return {string}
   */
  static get MESSAGES_GET() {
    return 'messages.get';
  }

  /**
   * MESSAGES_OBSERVE.
   *
   * @return {string}
   */
  static get MESSAGES_OBSERVE() {
    return 'messages.observe';
  }
};
