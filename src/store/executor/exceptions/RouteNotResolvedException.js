/**
 * RouteNotResolvedException.
 */
export class RouteNotResolvedException extends Error {
  /**
   * Constructor.
   */
  constructor() {
    super();

    if (Error.captureStackTrace) {
      Error.captureStackTrace(this, RouteNotResolvedException);
    }

    this.name = 'RouteNotResolvedException';
  }
}
