/**
 * MultipleRouteOneTypeNotSupportedException.
 */
export class MultipleRouteOneTypeNotSupportedException extends Error {
  /**
   * Constructor.
   */
  constructor() {
    super();

    if (Error.captureStackTrace) {
      Error.captureStackTrace(this, MultipleRouteOneTypeNotSupportedException);
    }

    this.name = 'MultipleRouteOneTypeNotSupportedException';
  }
}
