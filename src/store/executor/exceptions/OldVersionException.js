/**
 * OldVersionException.
 */
export class OldVersionException extends Error {
  /**
   * Constructor.
   */
  constructor() {
    super();

    if (Error.captureStackTrace) {
      Error.captureStackTrace(this, OldVersionException);
    }

    this.name = 'OldVersionException';
  }
}
