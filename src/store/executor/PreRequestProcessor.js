import Routes from './Routes';
import store from '../index';
import {PreparedRequestDto} from './requests/PreparedRequestDto';
import Route from './Routes';

/**
 * PreRequestProcessor.
 */
export default class PreRequestProcessor {
  /**
   * PreparedRequestsDto.
   *
   * @type {PreparedRequestsDto}
   */
  preparedRequests = null;
  /**
   * Constructor.
   *
   * @param {PreparedRequestsDto} preparedRequests
   */
  constructor(preparedRequests) {
    this.preparedRequests = preparedRequests;
  }

  /**
   * Create instance and apply rules.
   *
   * @param {PreparedRequestsDto} preparedRequests
   */
  static async apply(preparedRequests) {
    const preRequestProcessor = new PreRequestProcessor(preparedRequests);
    await preRequestProcessor.apply();
  }

  /**
   * Apply pre request rules.
   */
  async apply() {
    if (!this.preparedRequests) {
      return;
    }

    for (const request of this.preparedRequests.methods) {
      this.applyDestroyRules(request);
      await this.applyVersionRules(request);

      if (request.isRoute(Routes.MESSAGES_GET)) {
        this.applyMessagesGet(request);
      } else if (request.isRoute(Routes.MESSAGES_OBSERVE)) {
        this.applyMessagesObserve(request);
      }
    }
  }

  /**
   * Apply version rules.
   *
   * @param {PreparedRequestDto} request
   */
  async applyVersionRules(request) {
    request.setVersion(await store.dispatch('core/initAndGetDataVersion', request.storeVersionName));
  }

  /**
   * Apply destroy rules.
   *
   * @param {PreparedRequestDto} request
   */
  applyDestroyRules(request) {
    if (request.destroy) {
      this.preparedRequests.removeRequest(request);
    }
  }

  /**
   * Apply messages get rules.
   *
   * @param {PreparedRequestDto} request
   */
  applyMessagesGet(request) {
    if (!store.state.messages.currentChat) {
      this.preparedRequests.removeRequest(request);
    } else {
      this.extendUpdates(request);
      this.updateMessagesObserve(request);
    }
  }

  /**
   * Extend updates.
   * If specified updateAfter, last id changes to newest.
   *
   * @param {PreparedRequestDto} request
   */
  extendUpdates(request) {
    if (request.options.updateAfter === true) {
      if (request.parameters.length === 1 ||
        (request.parameters.length > 1 && request.parameters[1] === 1)
      ) {
        const existentMessages = store.state.messages.chatMessages;

        if (existentMessages && existentMessages.length > 0) {
          if (request.parameters.length === 1) {
            request.parameters[1] = 1;
          }

          request.parameters[2] = existentMessages[existentMessages.length - 1].id;
        } else {
          request.parameters = [
            request.parameters[0],
            1,
          ];
        }
      }
    }
  }

  /**
   * Update message observe rules.
   *
   * @param {PreparedRequestDto} request
   */
  updateMessagesObserve(request) {
    if (request.options.updateAfter !== true) {
      return;
    }

    const existentMessages = store.state.messages.chatMessages;

    if (existentMessages && existentMessages.length > 0) {
      if (this.preparedRequests.findByRoute(Route.MESSAGES_OBSERVE).length === 0) {
        const parameters = [existentMessages[0].id];

        if (existentMessages.length > 1) {
          parameters.push(existentMessages[existentMessages.length - 1].id);
        }

        this.preparedRequests.addOrReplace(new PreparedRequestDto(
            Route.MESSAGES_OBSERVE,
            parameters
        ));
      }
    }
  }

  /**
   * Apply message observe rules.
   *
   * @param {PreparedRequestDto} request
   */
  applyMessagesObserve(request) {
    const existentMessages = store.state.messages.chatMessages;
    const chat = store.state.messages.currentChat;

    if (chat && existentMessages && existentMessages.length > 0) {
      const parameters = [existentMessages[0].id];

      if (existentMessages.length > 1) {
        parameters.push(existentMessages[existentMessages.length - 1].id);
      }

      request.parameters = parameters;
    } else {
      this.preparedRequests
          .removeByRoute(Routes.MESSAGES_OBSERVE);
    }
  }
};
