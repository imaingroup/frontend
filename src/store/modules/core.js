import {fetchRequest} from '../helpers/fetchRequest';
import url from '../config/url';
// eslint-disable-next-line no-unused-vars
import {PreparedRequestDto} from '../executor/requests/PreparedRequestDto';
// eslint-disable-next-line no-unused-vars
import {PreparedRequestsDto} from '../executor/requests/PreparedRequestsDto';
import processorResolver from '../executor/processors/ProcessorResolver';
import {RouteNotResolvedException} from '../executor/exceptions/RouteNotResolvedException';
import {OldVersionException} from '../executor/exceptions/OldVersionException';
import PreRequestProcessor from '../executor/PreRequestProcessor';
import Logger from '../../logger/Logger';

/**
 * Core store module.
 * This module execute requests
 * to server and make auto updates.
 */
export const core = {
  namespaced: true,
  state: {
    versions: {},
    updateRequestsDto: null,
    enableOldAutoUpdates: false,
  },
  mutations: {
    setDataVersion: (state, {name, version}) => {
      state.versions[name] = version;
    },
    /**
     * Set update requests dto.
     *
     * @param {Object} state
     * @param {PreparedRequestsDto} requests
     */
    setUpdateRequestsDto: (state, requests) => {
      state.updateRequestsDto = requests;
    },
    /**
     * Set update requests dto.
     *
     * @param {Object} state
     */
    coreDisableAutoUpdate: (state) => {
      state.updateRequestsDto = null;
    },
    /**
     * Set update requests dto.
     *
     * @param {Object} state
     */
    coreEnableOldAutoUpdate: (state) => {
      state.enableOldAutoUpdates = true;
    },
    /**
     * Set update requests dto.
     *
     * @param {Object} state
     */
    coreDisableOldAutoUpdate: (state) => {
      state.enableOldAutoUpdates = false;
    },
  },
  actions: {
    /**
     * Get or init data version.
     *
     * @param {Object} store
     * @param {Function} store.commit
     * @param {Object} store.state
     * @param {String} name
     * @return {Number}
     */
    initAndGetDataVersion({commit, state}, name) {
      if (!state.versions[name]) {
        commit('setDataVersion', {name, version: 1});
      }

      return state.versions[name];
    },

    /**
     * Fetch data.
     *
     * @param {Object} store
     * @param {Function} store.commit
     * @param {PreparedRequestsDto} preparedRequests
     * @param {Boolean} disable403
     * @param {Boolean} isAutoUpdate
     * @return {Promise<Object>}
     */
    async getApiData({
      commit,
    }, {preparedRequests, disable403, isAutoUpdate}) {
      return await fetchRequest(
          url.concat('/data'),
          {
            method: 'POST',
            body: JSON.stringify(preparedRequests),
          },
          {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
          },
          disable403 === true,
          isAutoUpdate === true
      );
    },

    /**
     * Execute data requests.
     *
     * @param {Object} context
     * @param {Boolean} autoUpdate
     * @param {Boolean} executeNow
     * @param {Boolean} nowAlwaysFetchData
     * @param {PreparedRequestDto[]} preparedRequests
     * @return {Promise<Object|null>}
     */
    async tryExecuteDataRequests(context, {autoUpdate, executeNow, nowAlwaysFetchData, preparedRequests}) {
      const preparedRequestsDto = new PreparedRequestsDto(nowAlwaysFetchData === true, preparedRequests);

      let data = null;

      if (executeNow === true) {
        try {
          data = await context.dispatch('execute', {preparedRequests: preparedRequestsDto});
        } catch (e) {
          await Logger.registerException(e);
        }
      }

      if (autoUpdate === true) {
        const update = context.state.updateRequestsDto === null;
        await context.commit('setUpdateRequestsDto', preparedRequestsDto);

        if (update) {
          setTimeout(() => {
            context.dispatch('autoUpdate');
          }, context.rootState.rateUpdate);
        }
      }

      return data;
    },

    /**
     * Auto update.
     *
     * @param {Object} context
     */
    async autoUpdate(context) {
      if (!context.state.updateRequestsDto) {
        return;
      }

      context.state.updateRequestsDto.changeAlways(false);

      try {
        await context.dispatch('execute', {preparedRequests: context.state.updateRequestsDto, isAutoUpdate: true});
      } catch (e) {
        await Logger.registerException(e);
      }

      setTimeout(() => {
        context.dispatch('autoUpdate');
      }, context.rootState.rateUpdate);
    },

    /**
     * Execute request to server.
     *
     * @param {Object} context
     * @param {PreparedRequestsDto} preparedRequests
     * @param {Boolean} isAutoUpdate
     * @return {Promise<Object>}
     */
    async execute(context, {preparedRequests, isAutoUpdate}) {
      await PreRequestProcessor.apply(preparedRequests);

      const result = await context.dispatch('getApiData', {
        'preparedRequests': preparedRequests,
        'disable403': isAutoUpdate === true,
        'isAutoUpdate': isAutoUpdate === true,
      });

      if (result.ok) {
        await context.dispatch('processResponse', {response: result.data.data, requests: preparedRequests});
      }

      return result;
    },

    /**
     * Process response.
     *
     * @param {Object} context
     * @param {Object} response
     * @param {PreparedRequestsDto} requests
     * @throws {Error}
     */
    async processResponse(context, {response, requests}) {
      Object.keys(response).forEach((key) => {
        try {
          const Resolver = processorResolver.resolve(key);
          const req = requests.findByRoute(key);

          if (req.length > 0) {
            new Resolver(response[key], req[0])
                .process();
          }
        } catch (e) {
          if (![RouteNotResolvedException.name, OldVersionException.name].includes(e.name)) {
            throw e;
          }
        }
      });
    },
  },
};
