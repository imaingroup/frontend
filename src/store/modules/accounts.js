// eslint-disable-next-line no-unused-vars
import {PreparedRequestDto} from '../executor/requests/PreparedRequestDto';
import Route from '../executor/Routes';
// eslint-disable-next-line no-unused-vars

export const accounts = {
  namespaced: true,
  actions: {
    /**
     * Prepare get account.
     *
     * @return {Promise<PreparedRequestDto>}
     */
    async prepareGetAccount() {
      return new PreparedRequestDto(
          Route.ACCOUNT_GLOBAL,
          [],
          {}
      );
    },
  },
};
