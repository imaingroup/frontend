import {fetchRequest} from '../helpers/fetchRequestMessages';
import {set} from '../helpers/mutations';
import url from '../config/url';
import store from '../../store/index';
import createMutationsSharer from 'vuex-shared-mutations';
import {PreparedRequestDto} from '../executor/requests/PreparedRequestDto.js';
import Route from '../executor/Routes';

export const messages = {
  namespaced: true,
  state: {
    legends: null,
    contacts: null,
    chats: null,
    chatsOwner: null,
    currentChat: null,
    currentMessage: null,
    chatMessages: null,
    loadingMessages: false,
    loadingChats: false,
    isLoading: false,
    startingConversation: false,
  },
  mutations: {
    setLegends: set('legends'),
    setContacts: set('contacts'),
    setChats: (state, data) => {
      state.chats = data;

      if (state.currentChat) {
        data.forEach((el) => {
          if (el.id === state.currentChat.id) {
            state.currentChat = el;
          }
        });
      }

      if (store.state.user) {
        state.chatsOwner = data.filter((chat) => {
          return chat.users ? chat.users
              .find((user) => user.username === store.state.user.username && user.is_owner) : false;
        }).map((chat) => chat.id);
      }
    },
    setChat: set('currentChat'),
    setMessage: set('currentMessage'),
    setMessages: set('chatMessages'),
    clearAll: (state) => {
      state.contacts = null;
      state.chats = null;
      state.chatsOwner = null;
      state.currentChat = null;
      state.chatMessages = null;
    },
    setLoadingMessages: set('loadingMessages'),
    setLoadingChats: set('loadingChats'),
    setIsLoading: set('isLoading'),
    setStartingConversation: set('startingConversation'),
  },
  actions: {
    async prepareGetContacts() {
      return new PreparedRequestDto(
          Route.MESSAGES_CONTACTS_GET,
          {},
          {}
      );
    },
    async prepareGetChats() {
      return new PreparedRequestDto(
          Route.MESSAGES_CHATS_GET,
          {},
          {}
      );
    },
    async prepareSendMessage(context, {chatId, message, parentId}) {
      const parameters = [chatId];

      if (parentId) {
        parameters.push(parentId);
      }

      return new PreparedRequestDto(
          Route.MESSAGES_SEND,
          parameters,
          {'message': message, 'parentId': parentId}
      );
    },
    async prepareGetMessages({
      commit,
    }, {chatId, type, message, updateAfter}) {
      const parameters = [chatId];

      if (Number.isInteger(type)) {
        parameters.push(type);
      }

      if (Number.isInteger(message)) {
        parameters.push(message);
      }

      return new PreparedRequestDto(
          Route.MESSAGES_GET,
          parameters,
          {'order': 'ASC'},
          {'updateAfter': updateAfter === true}
      );
    },
    async fetchContacts({
      commit,
    }) {
      commit('setIsLoading', true);
      const result = await fetchRequest(
          url.concat('/messages/contacts/get'),
      );
      commit('setIsLoading', false);
      const m = 'fetchContacts';
      if (result.status === 200) {
        commit('setLegends', result.data.data.contacts.legend);
        commit('setContacts', result.data.data.contacts.chats);
        return true;
      } else if (result.status !== 403) {
        console.log(`ERROR: ${m}: ${result.status}`);
      }
    },
    async fetchChats({
      commit,
    }) {
      commit('setIsLoading', true);
      commit('setLoadingChats', true);
      const result = await fetchRequest(
          url.concat('/messages/chats/get'),
      );
      commit('setLoadingChats', false);
      const m = 'fetchChats';
      if (result.status === 200) {
        commit('setChats', result.data.data.chats);
        commit('setIsLoading', false);
        return true;
      } else if (result.status !== 403) {
        console.log(`ERROR: ${m}: ${result.status}`);
      }
    },
    async fetchMessages({
      commit,
    }, {chat, type, message, loading = true}) {
      if (loading) commit('setLoadingMessages', true);
      const result = await fetchRequest(
          url.concat(`/messages/get/${chat}${message ? '/' + type : ''}${message ? '/' + message : ''}/?order=ASC `),
          {},
          {},
          true
      );

      if (loading) commit('setLoadingMessages', false);

      if (result.status === 200) {
        return result.data.data.messages;
      } else if (result.status === 403) {
        commit('setChat', null);
      }

      return [];
    },
    async startConversation({
      commit,
    }, {contactId}) {
      return await fetchRequest(
          url.concat(`/messages/conversation/start/${contactId}`),
          {
            method: 'POST',
          },
      );
    },
    async sendMessage({
      commit,
    }, {chat, parent, message}) {
      commit('setIsLoading', true);

      const result = await fetchRequest(
          url.concat(`/messages/send/${chat}${parent ? '/' + parent : ''}`),
          {
            method: 'POST',
            body: JSON.stringify({message}),
          },
          {
            'Content-Type': 'application/json',
          }
      );

      if (result.status === 200 || result.status !== 422) {
        commit('setIsLoading', false);

        if (result.status === 200) {
          return result.data.data;
        }
      }

      return false;
    },
    async editMessage({
      commit,
    }, message) {
      commit('setIsLoading', true);
      const result = await fetchRequest(
          url.concat(`/messages/message/${message.id}/edit`),
          {
            method: 'POST',
            body: JSON.stringify({message_text: message.text}),
          },
          {
            'Content-Type': 'application/json',
          }
      );
      if (result.status === 200) {
        commit('setIsLoading', false);
        return true;
      } else if (result.status === 422) {
        commit('setIsLoading', false);
        return result.data.data;
      } else {
        return false;
      }
    },
    async removeMessage({
      commit,
    }, {message, forAll}) {
      commit('setIsLoading', true);
      const result = await fetchRequest(
          url.concat(`/messages/message/${message}/delete`),
          {
            method: 'POST',
            body: JSON.stringify(forAll ? {for_all: true} : null),
          },
          {
            'Content-Type': 'application/json',
          }
      );
      const m = 'removeMessage';
      if (result.status === 200) {
        commit('setIsLoading', false);
        return true;
      } else if (result.status === 422) {
        commit('setIsLoading', false);
        return result.data.data;
      } else if (result.status !== 403) {
        console.log(`ERROR: ${m}: ${result.status}`);
      }
    },
    async inviteUser({
      commit,
    }, id) {
      commit('setIsLoading', true);
      const result = await fetchRequest(
          url.concat(`/messages/contacts/add/${id}`),
          {
            method: 'POST',
          },
          {
            'Content-Type': 'application/json',
          }
      );
      if (result.status === 200) {
        commit('setIsLoading', false);
        return true;
      } else {
        return false;
      }
    },
    async acceptInvite({
      commit,
    }, chat) {
      commit('setIsLoading', true);
      const result = await fetchRequest(
          url.concat(`/messages/invitation/${chat}/accept`),
          {
            method: 'POST',
          },
          {
            'Content-Type': 'application/json',
          }
      );
      const m = 'acceptInvite';
      if (result.status === 200) {
        commit('setIsLoading', false);
        return true;
      } else if (result.status !== 403) {
        console.log(`ERROR: ${m}: ${result.status}`);
      }
    },
    async removeContact({
      commit,
    }, contact) {
      commit('setIsLoading', true);
      const result = await fetchRequest(
          url.concat(`/messages/contact/${contact}/delete`),
          {
            method: 'POST',
          },
          {
            'Content-Type': 'application/json',
          }
      );
      const m = 'removeContact';
      if (result.status === 200) {
        commit('setIsLoading', false);
        return true;
      } else if (result.status !== 403) {
        console.log(`ERROR: ${m}: ${result.status}`);
      }
    },
    async createChat({
      commit,
    }, chat) {
      commit('setIsLoading', true);
      const result = await fetchRequest(
          url.concat('/messages/group-chat/create'),
          {
            method: 'POST',
            body: chat,
          },
      );
      const m = 'createChat';
      if (result.status === 200) {
        commit('setIsLoading', false);
        return result.data.data.chat_id;
      } else if (result.status === 422) {
        commit('setIsLoading', false);
        return result.data.data;
      } else if (result.status !== 403) {
        console.log(`ERROR: ${m}: ${result.status}`);
      }
    },
    async editChat({
      commit,
    }, chat) {
      commit('setIsLoading', true);
      const result = await fetchRequest(
          url.concat(`/messages/group-chat/edit/${chat.id}`),
          {
            method: 'POST',
            body: chat.form,
          },
      );
      const m = 'editChat';
      if (result.status === 200) {
        commit('setIsLoading', false);
        return true;
      } else if (result.status === 422) {
        commit('setIsLoading', false);
        return result.data.data;
      } else if (result.status !== 403) {
        console.log(`ERROR: ${m}: ${result.status}`);
      }
    },
    async removeChat({
      commit,
    }, chat) {
      commit('setIsLoading', true);
      const result = await fetchRequest(
          url.concat(`/messages/chat/${chat}/delete`),
          {
            method: 'POST',
          },
          {
            'Content-Type': 'application/json',
          }
      );
      const m = 'removeChat';
      if (result.status === 200) {
        commit('setIsLoading', false);
        return true;
      } else if (result.status !== 403) {
        console.log(`ERROR: ${m}: ${result.status}`);
      }
    },
    async leaveChat({
      commit,
    }, chat) {
      commit('setIsLoading', true);
      const result = await fetchRequest(
          url.concat(`/messages/group-chat/leave/${chat}`),
          {
            method: 'POST',
          },
          {
            'Content-Type': 'application/json',
          }
      );
      const m = 'leaveChat';
      if (result.status === 200) {
        commit('setIsLoading', false);
        return true;
      } else if (result.status !== 403) {
        console.log(`ERROR: ${m}: ${result.status}`);
      }
    },
    async clearChat({
      commit,
    }, chat) {
      commit('setIsLoading', true);
      const result = await fetchRequest(
          url.concat(`/messages/${chat}/clean`),
          {
            method: 'POST',
          },
          {
            'Content-Type': 'application/json',
          }
      );
      const m = 'clearChat';
      if (result.status === 200) {
        commit('setIsLoading', false);
        return true;
      } else if (result.status !== 403) {
        console.log(`ERROR: ${m}: ${result.status}`);
      }
    },
    async addUser({
      commit,
    }, {chat, contact}) {
      commit('setIsLoading', true);
      const result = await fetchRequest(
          url.concat(`/messages/group-chat/users/add/${chat}/${contact}`),
          {
            method: 'POST',
          },
          {
            'Content-Type': 'application/json',
          }
      );
      const m = 'addUser';
      if (result.status === 200) {
        commit('setIsLoading', false);
        return true;
      } else if (result.status === 422) {
        commit('setIsLoading', false);
        return result.data.data;
      } else if (result.status !== 403) {
        console.log(`ERROR: ${m}: ${result.status}`);
      }
    },
    async removeUser({
      commit,
    }, {chat, contact}) {
      commit('setIsLoading', true);
      const result = await fetchRequest(
          url.concat(`/messages/group-chat/users/delete/${chat}/${contact}`),
          {
            method: 'POST',
          },
          {
            'Content-Type': 'application/json',
          }
      );
      const m = 'removeUser';
      if (result.status === 200) {
        commit('setIsLoading', false);
        return true;
      } else if (result.status !== 403) {
        console.log(`ERROR: ${m}: ${result.status}`);
      }
    },
    async blockUser({
      commit,
    }, promoCode) {
      commit('setIsLoading', true);
      const result = await fetchRequest(
          url.concat(`/messages/user/${promoCode}/block`),
          {
            method: 'POST',
          },
          {
            'Content-Type': 'application/json',
          }
      );
      const m = 'blockUser';
      if (result.status === 200) {
        commit('setIsLoading', false);
        return true;
      } else if (result.status !== 403) {
        console.log(`ERROR: ${m}: ${result.status}`);
      }
    },
    async unblockUser({
      commit,
    }, id) {
      commit('setIsLoading', true);
      const result = await fetchRequest(
          url.concat(`/messages/contact/${id}/unblock`),
          {
            method: 'POST',
          },
          {
            'Content-Type': 'application/json',
          }
      );
      const m = 'unblockUser';
      if (result.status === 200) {
        commit('setIsLoading', false);
        return true;
      } else if (result.status !== 403) {
        console.log(`ERROR: ${m}: ${result.status}`);
      }
    },
    async searchMessage({
      commit,
    }, {chat, search, loading = false}) {
      commit('setIsLoading', true);
      if (loading) commit('setLoadingChats', true);
      const result = await fetchRequest(
          url.concat(`/messages/search${chat ? '/' + chat : ''}/?search=${search}`),
      );
      if (loading) commit('setLoadingChats', false);
      const m = 'searchMessage';
      if (result.status === 200) {
        commit('setIsLoading', false);
        return result.data.data;
      } else if (result.status !== 403) {
        console.log(`ERROR: ${m}: ${result.status}`);
      }
    },
  },
  plugins: [
    createMutationsSharer({
      predicate: [
        'setContacts',
        'setChats',
        'setChat',
        'setMessages',
      ],
    }),
  ],
};
