import router from '../router';
import {NotFoundActionsException} from '../exceptions/actions/NotFoundActionsException';

import {fetchRequest} from './helpers/fetchRequest';
import url from './config/url';

const setLang = async ({
  commit,
}, lang) => {
  commit('setLang', lang);
  fetchRequest(
      url.concat('/language/set'),
      {
        method: 'POST',
        body: JSON.stringify({lang: lang}),
      },
      {
        'Content-Type': 'application/json',
      }
  );
};

const addSubscribe = async ({}, email) => {
  const result = await fetchRequest(
      url.concat('/landing/subscribe'),
      {
        method: 'POST',
        body: JSON.stringify({email: email}),
      },
      {
        'Content-Type': 'application/json',
      }
  );
  if (result.status === 200) {
    return true;
  } else if (result.status == 422) {
    return result.data;
  }
};
const sendFeedback = async ({}, form) => {
  const result = await fetchRequest(
      url.concat('/landing/contact'),
      {
        method: 'POST',
        body: JSON.stringify(form),
      },
      {
        'Content-Type': 'application/json',
      }
  );
  if (result.status === 200) {
    return true;
  } else if (result.status == 422) {
    return result.data;
  }
};
const exchangeUsdtAbc = async ({}, form) => {
  const result = await fetchRequest(
      url.concat('/exchange/usdt/abc'),
      {
        method: 'POST',
        body: JSON.stringify(form),
      },
      {
        'Content-Type': 'application/json',
      }
  );
  if (result.status === 200) {
    return true;
  } else if (result.status == 422) {
    return result.data;
  }
};
const exchangeAbcUsdt = async ({}, form) => {
  const result = await fetchRequest(
      url.concat('/exchange/abc/usdt'),
      {
        method: 'POST',
        body: JSON.stringify(form),
      },
      {
        'Content-Type': 'application/json',
      }
  );
  if (result.status === 200) {
    return true;
  } else if (result.status == 422) {
    return result.data;
  }
};
const profitCardBuy = async ({}, cardId) => {
  const result = await fetchRequest(
      url.concat('/profit-card/buy'),
      {
        method: 'POST',
        body: JSON.stringify({card_id: cardId}),
      },
      {
        'Content-Type': 'application/json',
      }
  );
  if (result.status === 200) {
    return true;
  } else if (result.status == 422) {
    return result.data;
  }
};
const openTid = async ({}, tid) => {
  const result = await fetchRequest(
      url.concat('/investments/open'),
      {
        method: 'POST',
        body: JSON.stringify(tid),
      },
      {
        'Content-Type': 'application/json',
      }
  );
  if (result.status === 200) {
    return true;
  } else if (result.status == 422) {
    return result.data;
  }
};
const depositMake = async ({}, form) => {
  const result = await fetchRequest(
      url.concat('/deposit/make'),
      {
        method: 'POST',
        body: JSON.stringify(form),
      },
      {
        'Content-Type': 'application/json',
      }
  );
  if (result.status === 200) {
    return true;
  } else if (result.status == 422) {
    return result.data;
  }
};
const depositMakeNew = async ({}, form) => {
  const result = await fetchRequest(
      url.concat('/deposit/make/new'),
      {
        method: 'POST',
        body: JSON.stringify(form),
      },
      {
        'Content-Type': 'application/json',
      }
  );
  if (result.status === 200) {
    return true;
  } else if (result.status == 422) {
    return result.data;
  }
};
const withdrawCreate = async ({}, form) => {
  const result = await fetchRequest(
      url.concat('/withdraw/create'),
      {
        method: 'POST',
        body: JSON.stringify(form),
      },
      {
        'Content-Type': 'application/json',
      }
  );
  if (result.status === 200) {
    return true;
  } else if (result.status == 422) {
    return result.data;
  }
};
const profileAccountSave = async ({}, form) => {
  const result = await fetchRequest(
      url.concat('/profile/account/save'),
      {
        method: 'POST',
        body: form,
      },
      {
      }
  );
  if (result.status === 200) {
    return true;
  } else if (result.status == 422) {
    return result.data;
  }
};
const profilePasswordChange = async ({}, form) => {
  const result = await fetchRequest(
      url.concat('/profile/password/change'),
      {
        method: 'POST',
        body: JSON.stringify(form),
      },
      {
        'Content-Type': 'application/json',
      }
  );
  if (result.status === 200) {
    return true;
  } else if (result.status == 422) {
    return result.data;
  }
};
const profileNotificationsSave = async ({}, form) => {
  const result = await fetchRequest(
      url.concat('/profile/notifications/save'),
      {
        method: 'POST',
        body: JSON.stringify(form),
      },
      {
        'Content-Type': 'application/json',
      }
  );
  if (result.status === 200) {
    return true;
  } else if (result.status == 422) {
    return result.data;
  }
};
const profile2faEnable = async ({
  commit,
}) => {
  const result = await fetchRequest(
      url.concat('/profile/2fa/enable'),
  );
  if (result.status === 200) {
    commit('set2fa', result.data.data);
    return true;
  } else if (result.status == 422) {
    return result.data;
  }
};
const profile2faDisable = async ({}, form) => {
  const result = await fetchRequest(
      url.concat('/profile/2fa/disable'),
      {
        method: 'POST',
        body: JSON.stringify(form),
      },
      {
        'Content-Type': 'application/json',
      }
  );
  if (result.status === 200) {
    return true;
  } else if (result.status == 422) {
    return result.data;
  }
};
const profile2faSet = async ({}, form) => {
  const result = await fetchRequest(
      url.concat('/profile/2fa/set'),
      {
        method: 'POST',
        body: JSON.stringify(form),
      },
      {
        'Content-Type': 'application/json',
      }
  );
  if (result.status === 200) {
    return true;
  } else if (result.status == 422) {
    return result.data;
  }
};

const fetchAccount = async ({
  commit,
}) => {
  const result = await fetchRequest(
      url.concat('/account/global'),
  );
  if (result.status === 200) {
    commit('setAccount', result.data.data);
    return true;
  }
};

const authCheck = async ({}) => {
  const result = await fetchRequest(
      url.concat('/account/auth/check'),
  );
  if (result.status === 200) {
    return result.data.data;
  }
};

const accountCheckUpdate = async ({}, {mask, version}) => {
  const result = await fetchRequest(
      url.concat('/account/check/update'),
      {
        method: 'POST',
        body: JSON.stringify({'m': mask, 'version': version}),
      },
      {
        'Content-Type': 'application/json',
      },
  );
  if (result.status === 200) {
    return result.data;
  }
};

const makeNotificationAsRead = async ({}, notificationId) => {
  const result = await fetchRequest(
      url.concat('/account/notification/view'),
      {
        method: 'POST',
        body: JSON.stringify({notification_id: notificationId}),
      },
      {
        'Content-Type': 'application/json',
      }
  );
  if (result.status === 200) {
    return true;
  }
};
const clearNotifications = async ({}) => {
  const result = await fetchRequest(
      url.concat('/account/notifications/clear'),
      {
        method: 'POST',
      },
  );
  if (result.status === 200) {
    return true;
  }
};
const newsItemLike = async ({}, id) => {
  const result = await fetchRequest(
      url.concat('/news/item/' + id + '/like'),
      {
        method: 'POST',
      },
      {
        'Content-Type': 'application/json',
      }
  );
  if (result.status === 200) {
    return true;
  } else if (result.status == 422) {
    return result.data;
  }
  return false;
};

const register = async ({}, user) => {
  const result = await fetchRequest(
      url.concat('/register/send'),
      {
        method: 'POST',
        body: JSON.stringify(user),
      },
      {
        'Content-Type': 'application/json',
      }
  );
  if (result.status === 200) {
    return true;
  } else if (result.status == 422) {
    return result.data;
  }
};

const login = async ({
  dispatch, commit,
}, user) => {
  const result = await fetchRequest(
      url.concat('/login'),
      {
        method: 'POST',
        body: JSON.stringify(user),
      },
      {
        'Content-Type': 'application/json',
      }
  );
  if (result.status === 200) {
    commit('setDashboardContents', null);
    commit('setDepositInfo', {});
    commit('setWithdrawInfo', {});
    commit('setProfileContent', {});
    commit('setTransactions', null);
    commit('setNewsCategories', null);
    commit('setNews', null);
    commit('setNewsSingle', null);
    commit('setInvestments', null);
    commit('setInvestmentsBalances', {});
    commit('setOptions', []);
    commit('setInvestmentsChart', null);
    commit('setDeposits', null);
    commit('setWithdraws', null);
    commit('setTeam', null);
    dispatch('fetchAccount');

    return true;
  } else if (result.status == 422) {
    return result.data;
  }
};

const logout = async ({
  commit,
}, register = false) => {
  const result = await fetchRequest(
      url.concat('/logout'),
  );
  if (result.status === 200) {
    commit('clearAll');
    commit('messages/clearAll');

    if (!['Register', 'Login', 'Forgot', 'Reset', 'Activate'].includes(router.currentRoute.name)) {
      router.push({name: register ? 'Register' : 'Login'});
    }
    return true;
  }
};

const forgot = async ({}, user) => {
  const result = await fetchRequest(
      url.concat('/forgot/send'),
      {
        method: 'POST',
        body: JSON.stringify(user),
      },
      {
        'Content-Type': 'application/json',
      }
  );
  if (result.status === 200) {
    return true;
  } else if (result.status == 422) {
    return result.data;
  }
};

const reset = async ({}, data) => {
  const result = await fetchRequest(
      url.concat('/reset/' + data.token),
      {
        method: 'POST',
        body: JSON.stringify(data.password),
      },
      {
        'Content-Type': 'application/json',
      }
  );
  if (result.status === 200) {
    return true;
  } else if (result.status == 422) {
    return result.data;
  }
};

const resend = async ({}, data) => {
  const result = await fetchRequest(
      url.concat('/activation/resend'),
      {
        method: 'POST',
        body: JSON.stringify(data),
      },
      {
        'Content-Type': 'application/json',
      }
  );
  if (result.status === 200) {
    return true;
  } else if (result.status == 422) {
    return result.data;
  }
};

const activate = async ({}, token) => {
  const result = await fetchRequest(
      url.concat('/access/activate'),
      {
        method: 'POST',
        body: JSON.stringify({token: token}),
      },
      {
        'Content-Type': 'application/json',
      }
  );
  if (result.status === 200) {
    return true;
  } else if (result.status == 422) {
    return result.data;
  }
};

const fetchRegisterContents = async ({
  commit,
}) => {
  const result = await fetchRequest(
      url.concat('/register/content'),
  );
  if (result.status === 200) {
    commit('setTranslates', result.data.data.translates);
  }
};

const fetchLandingContents = async ({
  commit,
}) => {
  const result = await fetchRequest(
      url.concat('/landing/content'),
  );
  if (result.status === 200) {
    commit('setLandingContents', result.data.data);
  }
};
const fetchDashboardContents = async ({
  commit,
}) => {
  const result = await fetchRequest(
      url.concat('/dashboard/content'),
  );
  if (result.status === 200) {
    commit('setDashboardContents', result.data.data);
  }
};
const fetchDepositInfo = async ({
  commit,
}) => {
  const result = await fetchRequest(
      url.concat('/deposit/data'),
  );
  if (result.status === 200) {
    commit('setDepositInfo', result.data.data);
  }
};
const fetchWithdrawInfo = async ({
  commit,
}) => {
  const result = await fetchRequest(
      url.concat('/withdraw/info'),
  );
  if (result.status === 200) {
    commit('setWithdrawInfo', result.data.data);
  }
};
const fetchProfileContent = async ({
  commit,
}) => {
  const result = await fetchRequest(
      url.concat('/profile/content'),
  );
  if (result.status === 200) {
    commit('setProfileContent', result.data.data);
  }
};

const fetchTransactions = async ({
  commit,
}) => {
  const result = await fetchRequest(
      url.concat('/transactions'),
  );
  if (result.status === 200) {
    commit('setTransactions', result.data.data.transactions);
  }
};

const fetchNewsCategories = async ({
  commit,
}) => {
  const result = await fetchRequest(
      url.concat('/news/categories'),
  );
  if (result.status === 200) {
    commit('setNewsCategories', result.data.data.categories);
  }
};
const fetchNews = async ({
  commit,
  state: {newsParams},
}) => {
  Object.keys(newsParams)
      .forEach((key) => (
        newsParams[key] == null ||
        newsParams[key] == ''
      ) && delete newsParams[key]);
  const result = await fetchRequest(
      url.concat('/news/list?' + new URLSearchParams(newsParams).toString()),
  );
  if (result.status === 200) {
    commit('setNews', result.data.data.news);
  }
};
const fetchNewsSingle = async ({
  commit,
}, id) => {
  const result = await fetchRequest(
      url.concat('/news/item/' + id),
  );
  if (result.status === 200) {
    commit('setNewsSingle', result.data.data.news);
  }
};

const fetchInvestments = async ({
  commit,
}) => {
  const result = await fetchRequest(
      url.concat('/investments/history'),
  );
  if (result.status === 200) {
    commit('setInvestments', result.data.data.history);
  }
};
const fetchInvestmentsBalances = async ({
  commit,
}) => {
  const result = await fetchRequest(
      url.concat('/investments/balances'),
  );
  if (result.status === 200) {
    commit('setInvestmentsBalances', result.data.data.balances);
  }
};
const fetchOptions = async ({
  commit,
}) => {
  const result = await fetchRequest(
      url.concat('/investments/options'),
  );
  if (result.status === 200) {
    commit('setOptions', result.data.data.options);
  }
};
const fetchInvestmentsChart = async ({
  commit,
}, params) => {
  let chartUrl = url.concat('/investments/chart/v2');

  if (params && params.trade) {
    chartUrl = chartUrl.concat('/' + params.trade);
  }

  if (params && params.period) {
    chartUrl = chartUrl.concat('?period=' + params.period);
  }

  const result = await fetchRequest(
      chartUrl,
  );

  if (result.status === 200) {
    commit('setInvestmentsChart', result.data.data.chart);
  }
};

const fetchDeposits = async ({
  commit,
}) => {
  const result = await fetchRequest(
      url.concat('/deposit/history'),
  );
  if (result.status === 200) {
    commit('setDeposits', result.data.data);
  }
};

const fetchWithdraws = async ({
  commit,
}) => {
  const result = await fetchRequest(
      url.concat('/withdraw/history'),
  );
  if (result.status === 200) {
    commit('setWithdraws', result.data.data);
  }
};

const getTeam = async ({
  commit,
}) => {
  const result = await fetchRequest(
      url.concat('/team/get'),
  );
  if (result.status === 200) {
    commit('setTeam', result.data.data.team);
  }
};

const sendErrorLog = async ({}, log) => {
  await fetchRequest(
      url.concat('/log/send'),
      {
        method: 'POST',
        body: JSON.stringify({'log': log}),
      },
      {
        'Content-Type': 'application/json',
      }
  );
};

const identificationsSend = async ({}, data) => {
  const result = await fetchRequest(
      url.concat('/user/info'),
      {
        method: 'POST',
        body: JSON.stringify(data),
      },
      {
        'Content-Type': 'application/json',
      }
  );

  if (result.status === 200) {
    return true;
  } else if (result.status == 422) {
    return result.data;
  }
};

const fetchChartByOption = async ({}, params) => {
  let currentUrl = url.concat('/investments/chart/option/'+params['optionId']);

  if (params['period']) {
    currentUrl = currentUrl.concat('?period=' + params['period']);
  }

  const result = await fetchRequest(
      currentUrl,
  );

  if (result.status === 404) {
    throw new NotFoundActionsException('Chart by this option not found.');
  } else if (result.status === 200) {
    return result.data;
  }

  return null;
};

export {
  setLang,
  addSubscribe,
  sendFeedback,
  exchangeUsdtAbc,
  exchangeAbcUsdt,
  profitCardBuy,
  openTid,
  depositMake,
  depositMakeNew,
  withdrawCreate,
  profileAccountSave,
  profilePasswordChange,
  profileNotificationsSave,
  profile2faEnable,
  profile2faDisable,
  profile2faSet,

  fetchAccount,
  authCheck,
  accountCheckUpdate,

  makeNotificationAsRead,
  clearNotifications,
  newsItemLike,

  register,
  login,
  logout,
  forgot,
  reset,
  resend,
  activate,

  fetchRegisterContents,

  fetchLandingContents,
  fetchDashboardContents,
  fetchWithdrawInfo,
  fetchDepositInfo,
  fetchProfileContent,

  fetchTransactions,
  fetchNewsCategories,
  fetchNews,
  fetchNewsSingle,
  fetchInvestments,
  fetchInvestmentsBalances,
  fetchOptions,
  fetchInvestmentsChart,
  fetchDeposits,
  fetchWithdraws,
  getTeam,

  sendErrorLog,

  identificationsSend,

  fetchChartByOption,
};
